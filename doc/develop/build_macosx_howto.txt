**************************************************************************
Install third-party tools
**************************************************************************
export MACOSX_DEPLOYMENT_TARGET=10.12
sudo echo "macos_deployment_target 10.12" >> /opt/local/etc/macports/macports.conf
sudo port install cmake docbook-xml doxygen libxslt boost yasm povray
sudo port install -s fftw-3 libssh

**************************************************************************
Install Qt libraries, version 5.9.7
**************************************************************************

# This is a workaround to let VS Code Intellisense find the Qt headers:
cd $HOME/Qt/5.9.7/clang_64/
mkdir headers
cd headers
ln -s $PWD/../lib/QtCore.framework/Versions/5/Headers/ QtCore
ln -s $PWD/../lib/QtGui.framework/Versions/5/Headers/ QtGui
ln -s $PWD/../lib/QtWidgets.framework/Versions/5/Headers/ QtWidgets
ln -s $PWD/../lib/QtConcurrent.framework/Versions/5/Headers/ QtConcurrent
ln -s $PWD/../lib/QtNetwork.framework/Versions/5/Headers/ QtNetwork
ln -s $PWD/../lib/QtXml.framework/Versions/5/Headers/ QtXml
ln -s $PWD/../lib/QtOpenGL.framework/Versions/5/Headers/ QtOpenGL

**************************************************************************
Download and build SIP
**************************************************************************
python3 configure.py --deployment-target ${MACOSX_DEPLOYMENT_TARGET}
make -j4 install
python3 -m pip install PyQt5-sip

**************************************************************************
Download and build PyQt5
**************************************************************************
python3 configure.py \
  --confirm-license \
  --no-designer-plugin \
  --no-qml-plugin \
  --assume-shared \
  --qmake $HOME/Qt/5.9.7/clang_64/bin/qmake \
  --sip=/Library/Frameworks/Python.framework/Versions/3.7/bin/sip \
  --concatenate \
  --enable QtCore \
  --enable QtGui \
  --enable QtWidgets \
  --enable QtMacExtras && make -j4 && make install

**************************************************************************
Install Sphinx, Numpy, Matplotlib, IPython
**************************************************************************
python3 -m pip install sphinx
python3 -m pip install numpy
python3 -m pip install matplotlib
python3 -m pip install ipython

**************************************************************************
Disable user site packages
**************************************************************************
Edit "site.py" of the Python interpreter and set
  ENABLE_USER_SITE = False

**************************************************************************
Download and build QScintilla2
**************************************************************************
cd QScintilla_gpl-2.10.8/Qt4Qt5/
$HOME/Qt/5.9.7/clang_64/bin/qmake qscintilla.pro && \
make -j4 && \
install_name_tool -id $PWD/libqscintilla2_qt5.13.2.1.dylib libqscintilla2_qt5.13.2.1.dylib

**************************************************************************
Build static Libav:
**************************************************************************
cd libav-11.12
export MACOSX_DEPLOYMENT_TARGET=10.12
./configure \
  --disable-network \
  --disable-programs \
  --disable-debug \
  --disable-doc \
  --disable-filters \
  --disable-static \
  --enable-shared \
  --prefix=$HOME/progs/libav && \
make install      # Do not use parallel build!

**************************************************************************
Download and build HDF5:
**************************************************************************
mkdir hdf5_build
cd hdf5_build
cmake \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=../hdf5 \
  -DBUILD_SHARED_LIBS=ON \
  -DHDF5_ENABLE_Z_LIB_SUPPORT=ON \
  -DHDF5_BUILD_HL_LIB=ON \
  -DHDF5_BUILD_EXAMPLES=OFF \
  -DHDF5_BUILD_TOOLS=ON \
  -DHDF5_BUILD_WITH_INSTALL_NAME=ON \
  -DBUILD_TESTING=OFF \
  ../hdf5-1.10.2
make -j4 install

**************************************************************************
Download and build NetCDF:
**************************************************************************
mkdir netcdf_build
cd netcdf_build
cmake \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=../netcdf \
  -DENABLE_DAP=OFF \
  -DENABLE_EXAMPLES=OFF \
  -DENABLE_TESTS=OFF \
  -DBUILD_TESTING=OFF \
  -DBUILD_UTILITIES=OFF \
  -DENABLE_HDF4=OFF \
  -DUSE_HDF5=ON \
  -DHDF5_DIR=$HOME/progs/hdf5/share/cmake/hdf5 \
  ../netcdf-4.6.1/
make -j4 install
cd ../netcdf/lib
install_name_tool -id $PWD/libnetcdf.dylib libnetcdf.dylib

**************************************************************************
Download Intel TBB
**************************************************************************
install_name_tool -id $PWD/libtbb.dylib libtbb.dylib
install_name_tool -id $PWD/libtbbmalloc.dylib libtbbmalloc.dylib

**************************************************************************
Download and build Intel Embree
**************************************************************************
cd /Users/stuko/progs/
git clone https://github.com/embree/embree.git
cd embree
git checkout v3.5.2
mkdir build
cd build
cmake -G "Xcode" \
      -DCMAKE_INSTALL_PREFIX=$HOME/progs/embree_install \
      -DEMBREE_ISPC_EXECUTABLE=$HOME/progs/ispc-v1.9.2-osx/ispc \
      -DEMBREE_TBB_ROOT=$HOME/progs/tbb2019_20190320oss \
      -DTBB_INCLUDE_DIR=$HOME/progs/tbb2019_20190320oss/include/ \
      -DTBB_LIBRARY=$HOME/progs/tbb2019_20190320oss/lib/libtbb.dylib \
      -DTBB_LIBRARY_MALLOC=$HOME/progs/tbb2019_20190320oss/lib/libtbbmalloc.dylib \
      -DEMBREE_TUTORIALS=OFF \
      -DCMAKE_OSX_DEPLOYMENT_TARGET=10.12 \
      ..
cmake --build . --config Release --target install

**************************************************************************
Download and build Intel OSPRay
**************************************************************************
cd /Users/stuko/progs/
git clone https://github.com/ospray/ospray.git
cd ospray
git checkout v1.8.4
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/progs/ospray_install \
      -DISPC_EXECUTABLE=$HOME/progs/ispc-v1.9.2-osx/ispc \
      -Dembree_DIR=$HOME/progs/embree_install/lib/cmake/embree-3.5.2 \
      -DTBB_ROOT=$HOME/progs/tbb2019_20190320oss \
      -DBUILD_SHARED_LIBS=ON \
      -DCMAKE_CXX_STANDARD=14 \
      -DOSPRAY_ENABLE_TUTORIALS=OFF \
      -DCMAKE_OSX_DEPLOYMENT_TARGET=10.12 \
      ..
make -j4 install
cd ../../ospray_install/lib
install_name_tool -id $PWD/libospray.0.dylib libospray.0.dylib
install_name_tool -change @rpath/libospray_common.0.dylib $PWD/libospray_common.0.dylib libospray.0.dylib
install_name_tool -id $PWD/libospray_common.0.dylib libospray_common.0.dylib
install_name_tool -id $PWD/libospray_module_ispc.0.dylib libospray_module_ispc.0.dylib
install_name_tool -change @rpath/libospray.0.dylib $PWD/libospray.0.dylib libospray_module_ispc.0.dylib
install_name_tool -change @rpath/libospray_common.0.dylib $PWD/libospray_common.0.dylib libospray_module_ispc.0.dylib

**************************************************************************
Build OVITO:
**************************************************************************
cmake -DOVITO_BUILD_DOCUMENTATION=ON \
      -DCMAKE_OSX_DEPLOYMENT_TARGET=10.12 \
      -DCMAKE_BUILD_TYPE=Release \
      -DOVITO_REDISTRIBUTABLE_PACKAGE=ON \
      -DOVITO_DOUBLE_PRECISION_FP=ON \
      -DCMAKE_INSTALL_PREFIX=../release_install \
      -DCMAKE_PREFIX_PATH=$HOME/Qt/5.9.7/clang_64/ \
      -DPYTHON_EXECUTABLE=/Library/Frameworks/Python.framework/Versions/3.7/bin/python3 \
      -DPYTHON_INCLUDE_DIR=/Library/Frameworks/Python.framework/Versions/3.7/include/python3.7m \
      -DPYTHON_LIBRARY=/Library/Frameworks/Python.framework/Versions/3.7/lib/libpython3.7.dylib \
      -DLIBAV_INCLUDE_DIR=$HOME/progs/libav/include \
      -DLIBAV_LIBRARY_DIR=$HOME/progs/libav/lib \
      -DQSCINTILLA_INCLUDE_DIR=$HOME/progs/QScintilla_gpl-2.10.8/Qt4Qt5 \
      -DQSCINTILLA_LIBRARY=$HOME/progs/QScintilla_gpl-2.10.8/Qt4Qt5/libqscintilla2_qt5.dylib \
      -DnetCDF_DIR=$HOME/progs/netcdf/lib/cmake/netCDF \
      -DHDF5_DIR=$HOME/progs/hdf5/share/cmake \
      -DOVITO_BUILD_PLUGIN_OSPRAY=ON \
      -Dospray_DIR=$HOME/progs/ospray_install/lib/cmake/ospray-1.8.4 \
      -Dembree_DIR=$HOME/progs/embree_install/lib/cmake/embree-3.5.2 \
      -DTBB_ROOT=$HOME/progs/tbb2019_20190320oss \
      -DISPC_EXECUTABLE=$HOME/progs/ispc-v1.9.2-osx/ispc \
      ../..