<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="particles.modifiers.affine_transformation"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>Affine transformation</title>

  <para>
    <informalfigure><screenshot><mediaobject><imageobject>
       <imagedata fileref="images/modifiers/affine_transformation_panel.png" format="PNG" scale="50" />
    </imageobject></mediaobject></screenshot></informalfigure>
    This modifier applies an affine transformation to the system. It may be used to translate, scale, rotate or shear 
    the particles, the simulation cell and/or other elements. The transformation can either be explicitly specified in terms of a 3 x 3 
    matrix plus a translation vector, or implicitly by prescribing a target geometry for the simulation cell.
  </para>
  
  <para>
    Given the 3 x 3 linear transformation matrix <inlineequation><mathphrase><emphasis>M</emphasis></mathphrase></inlineequation> 
    and the translation vector <inlineequation><mathphrase><emphasis>t</emphasis></mathphrase></inlineequation>, 
    which are both parameters of the modifier,
    the new position <inlineequation><mathphrase><emphasis>x'</emphasis></mathphrase></inlineequation> for
    a particle at the original position <inlineequation><mathphrase><emphasis>x</emphasis></mathphrase></inlineequation>
    is computed as
    <inlineequation><mathphrase><emphasis>x'</emphasis> = <emphasis>M</emphasis>&#x22C5;<emphasis>x</emphasis> + <emphasis>t</emphasis></mathphrase></inlineequation>.
    This notation uses column vectors.
  </para>
  
  <para>
    The <guibutton>Enter rotation</guibutton> button opens a dialog box letting you enter a rotation
    axis, a rotation angle and a center of rotation. Based on these inputs, OVITO will compute the corresponding 
    affine transformation matrix for you.
  </para>

  <simplesect>
    <title>Transform to target box</title>
    <para>
      This option lets the modifier automatically compute the affine transformation from the current shape of the input simulation cell and the specified 
      target shape. After application of the implicitly determined transformation, the cell will take on the 
      given target size and shape. The contents of the simulation cell (e.g. particles, surface meshes, etc.) will be mapped to the new
      cell shape accordingly, unless you turn their transformation off (see next section).
    </para>
    <para>
      Note that you can use the target box option as a way to replace the simulation cell read from the input simulation file(s)
      with a constant, non-varying simulation cell.
    </para>
  </simplesect>

  <simplesect>
    <title>Transformed elements</title>
    <para>
      You can select the kinds of elements that should be transformed by the modifier: 
      <informaltable>
        <tgroup cols="2">
          <thead>
            <row>
              <entry>Data element</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry>Particles</entry>
              <entry><para>Applies the affine transformation to the coordinates of the particles (<literal>Position</literal> particle property).</para></entry>
            </row>
            <row>
              <entry>Vector particle properties</entry>
              <entry><para>Applies the linear part of the affine transformation to the <literal>Velocity</literal>, <literal>Force</literal>
                    and <literal>Displacement</literal> particle properties.</para></entry>
            </row>
            <row>
              <entry>Simulation cell</entry>
              <entry><para>Applies the affine transformation to the origin of the <link linkend="scene_objects.simulation_cell">simulation cell</link> and the linear part of the transformation
                    to the three cell vectors.</para></entry>
            </row>
            <row>
              <entry>Surfaces</entry>
              <entry><para>Applies the affine transformation to the vertices of <link linkend="scene_objects.surface_mesh">surface meshes</link>.</para></entry>
            </row>
          </tbody>
        </tgroup>
      </informaltable>  
    </para>
    <para>
      The option <emphasis>Transform selected elements only</emphasis> restricts the application of the transformation to 
      the currently selected particles. 
    </para>
  </simplesect>

  <simplesect>
  <title>See also</title>
    <para>
      <link xlink:href="python/modules/ovito_modifiers.html#ovito.modifiers.AffineTransformationModifier"><classname>AffineTransformationModifier</classname> (Python API)</link>
    </para>
  </simplesect>
    
</section>
